# https://docs.vultr.com/how-to-deploy-fastapi-applications-with-gunicorn-and-nginx-on-ubuntu-20-04
# https://docs.gunicorn.org/en/stable/settings.html#on-starting

import sys, json, os, os.path
from multiprocessing import Process

#import debugpy
#debugpy.listen(('localhost', 5678))
#debugpy.wait_for_client()

from orm.database import Base, engine

import runner

sys.path.insert(0, os.path.join("..", "pkg"))
sys.path.insert(0, os.path.join("lib"))

with open(os.path.join("..", "etc", "infraops.json"), "r") as file:
    conf = json.load(file)

# Configurable Worker Options
bind = conf["web"]["bind"]
workers = conf["web"]["worker"]
timeout = conf["web"]["timeout"]
if "access" in conf["log"] and conf["log"]["access"]:
    accesslog = os.path.join(os.path.curdir, conf["log"]["access"])
if "info" in conf["log"] and conf["log"]["info"]:
    errorlog = os.path.join(os.path.curdir, conf["log"]["info"])
loglevel = conf["log"]["loglevel"]
capture_output = True

# Static Worker Options
wsgi_app = "web:app"
worker_class = "uvicorn.workers.UvicornWorker"

if not os.path.exists(os.path.join(os.path.curdir, conf["data"]["jobs"])):
    os.makedirs(os.path.join(os.path.curdir, conf["data"]["jobs"]))

p = Process(target=runner.main, kwargs={
    "data_base": os.path.join(os.path.curdir, conf["data"]["jobs"]),
    "web_bind": bind
})

def on_starting(server):
    Base.metadata.create_all(engine)
    
    p.start()

def on_exit(server):
    p.terminate()
    